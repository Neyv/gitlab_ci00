package myproject.calculation.impl;

import org.junit.Assert;
import org.junit.Test;

public class CalculatorImplTest {
    CalculatorImpl calculatorImpl = new CalculatorImpl();
    public static final double DELTA = 0.001;

    @Test
    public void testAdd() {
        double result = calculatorImpl.add(5.5, 6.8);
        Assert.assertEquals(12.3, result, DELTA);
    }

    @Test
    public void testSubtract() {
        double result = calculatorImpl.subtract(5.5, 6.8);
        Assert.assertEquals(-1.3, result, DELTA);
    }

    @Test
    public void testMultiply() {
        double result = calculatorImpl.multiply(18.3, 4);
        Assert.assertEquals(73.2, result, DELTA);
    }

    @Test
    public void testDivide() {
        double result = calculatorImpl.divide(15, 7.5);
        Assert.assertEquals(2, result, DELTA);
    }

    @Test
    public void testDivideByZero() {
        double result = calculatorImpl.divide(8, 0);
        Assert.assertTrue(Double.isInfinite(result));
    }

    @Test
    public void testDivideZeroByZero() {
        double result = calculatorImpl.divide(0, 0);
        Assert.assertTrue(Double.isNaN(result));
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme